// Bai 1:
function printTable(){
    var tableHTML = "";
    for (i = 1; i < 100; i+=10){
        var row = "";
        for (j = 0; j < 10; j++){
                row += (i+j) + " ";
            }
        tableHTML = tableHTML + row + "<br>";
        }
    document.getElementById("bai-1-result").innerHTML = `${tableHTML}`;
}


// Bai 2:
var numberArray = [];
var input = document.getElementById("txt-number");
input.addEventListener("keypress",function(event){
    if (event.key === "Enter") {
        event.preventDefault();
        document.getElementById("myBtn").click();
    }
});

function addNumberToArray(){
    var inputNumber = document.querySelector("#txt-number").value.trim() * 1;
    numberArray.push(inputNumber);
    document.querySelector("#txt-number").value = "";
    document.querySelector("#input-array").innerHTML = `<h3> Dãy Số: ${numberArray}</h3>`;
}
function checkPrimeNumber(num){
    var isPrime = false;
    if (num == 2){
        isPrime = true;
    } else if (num > 2){
        for (o = 2; o < num; o++){
            if(num % o == 0){
                isPrime = false;
                break;
            } else {
                isPrime = true;
            }
        }
    }
    return isPrime;
}

function findPrimeNumber(){
    var primeArray = [];
    for (z = 0; z < numberArray.length; z++){
        if(checkPrimeNumber(numberArray[z])){
           primeArray.push(numberArray[z]);
        }
    }
    console.log(primeArray)
    return primeArray;
}
function printPrimeNumber(){
    var primeArray = findPrimeNumber(numberArray);
    if (primeArray.length == 0){
        document.querySelector("#bai-2-result").innerHTML = `<p> -1 </p>`;
        return -1;
    } else {
        document.querySelector("#bai-2-result").innerHTML = `<p> Số Nguyên Tố: ${primeArray} </p>`;
    }
}

// Bai 3:
function calcSum(){
    var num = document.querySelector("#so-n").value * 1;
    var sum = 0;
    for (a = 2; a <=num; a++){
        sum +=a;
    }
    return sum + 2 * num;
}
function printSum(){
    document.querySelector("#bai-3-result").innerHTML = `Tổng S: ${calcSum()}`;
}

// Bai 4:
var n = 13;
function findDivisor(){
    var num = document.querySelector("#n").value * 1;
    divisorArray = [];
    for (k = num; k > 0; k--){
        if (num % k == 0){
            divisorArray.push(k);
        }
    }
    document.querySelector("#bai-4-result").innerHTML = `Ước của ${num}: ${divisorArray}`;
}

// Bai 5:
function reverseString(){
    var str = document.getElementById("tham-so-n").value;
    var newStr = "";
    for (c = str.length -1; c >=0; c--){
        newStr += str[c];
    }
    document.querySelector("#bai-5-result").innerHTML = `Dãy Đảo Ngược: ${newStr}`;
}

// Bai 6:
function findX(){
    var num = document.getElementById("tong").value * 1;
    var i = 0;
    var sum = 0;
    while(sum <= num){
        i++;
        sum += i;
    }
    document.querySelector("#bai-6-result").innerHTML = `X = ${i}`;
}

// Bai 7:
function printMultable(){
    var bangCuuChuongHTML = "";
    var num = document.getElementById("bang-cuu-chuong").value * 1;
    for ( e = 0; e <= 10; e++){
        bangCuuChuongHTML += `${num} x ${e} = ${num*e} <br>`;
    }
    document.querySelector("#bai-7-result").innerHTML = `${bangCuuChuongHTML}`;
}


// Bai 8:
var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S",
"AS", "7H", "9K", "10D"];
function dealCards(){
    var player1 = [], player2 = [], player3 = [], player4 = [];
    for (f = 0; f < cards.length; f++){
        var mod = f % 4;
        if (mod == 0){
            player1.push(cards[f]);
        } else if (mod == 1){
            player2.push(cards[f]);
        }
        else if (mod == 2){
            player3.push(cards[f]);
        } else {
            player4.push(cards[f]);
        }
    }
    document.querySelector("#bai-8-result").innerHTML = `Player1: ${player1}<br>Player2: ${player2}<br>Player3: ${player3}<br>Player4: ${player4}`;
}

// Bai 9:

function timSoGaCho(){
    var a = document.getElementById("so-con").value * 1;
    var b = document.getElementById("so-chan").value * 1;
    var soCho = b/2 - a;
    var soGa = a - soCho;
    console.log("Số Gà: ", soGa);
    console.log("Số Chó: ", soCho);
    document.querySelector("#bai-9-result").innerHTML = `Số gà: ${soGa} <br> Số chó: ${soCho}`;
}

// Bai 10:
function calcAngle(){
    var time = document.getElementById("gio-phut").value;
    if (time.length == 0){
        document.querySelector("#bai-10-result").innerHTML = `Vui lòng điền đầy đủ Giờ, Phút, AM-PM`;
    } else {
        timeSplit = time.split(':');
        h = timeSplit[0] * 1;
        m = timeSplit[1] * 1;
        if( h < 0 || m < 0 || h > 12 || m > 60){
            console.log("Wrong Input!");
        }
        if (h == 12){
            h = 0;
        }
        if (m == 60){
            m = 0;
            h += 1;
        }
        if (h > 12){
            h -= 12;
        }
        var hourAngle = (h * 60 + m)/2;
        var minuteAngle = m * 6;
        var angle = Math.abs(hourAngle - minuteAngle);
        if (angle > 180){
            angle = 360 - angle;
        }
    }
    document.querySelector("#bai-10-result").innerHTML = `Góc Giữa Kim Giờ & Kim Phút ${angle}`;
}

